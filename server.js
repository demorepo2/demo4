const express = require('express')
const cors = require ('cors')

const app = express()
app.use(cors('*'))
app.use(express.json())

app.get('/',(request,response)=>{
    response.send('welcome to the node v3')
})

app.listen(3000, '0.0.0.0',()=>{
    console.log('server started on 3000')
})